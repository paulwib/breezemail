<?php

/**
 * Breezemail tests for composing mail
 *
 * These tests are neccessarily simplified to check that generated
 * messages look roughly correct. Thorough testing requires sending
 * these files through various mail servers and viewing in various
 * clients to check compatibility.
 */

class BreezemailTest extends PHPUnit_Framework_TestCase {

    /**
     * Custom assert function that will test the header contents of
     * a Breezemail converted toString
     */
    function assertEmailHeader($str, $name, $value){
        // Parse the message headers from the string
        $tmp = explode("\r\n\r\n", $str);
        $header_str = array_shift($tmp);
        $headers = explode("\r\n", $header_str);
        foreach ($headers as $header) {
            list($tmp_name, $tmp_value) = explode(": ", $header);
            if ($tmp_name == $name) {
                $this->assertEquals($tmp_value, $value);
                return true;
            }
        }
        // If header not found this is an error
        $this->assertTrue(false, "Email header '$name' is not defined");
    }
    /**
     * A simple plain text email
     */
    function testSimplePlainText() {
        $email = new Breezemail();
        $email->id('test-text-email');
        $email->subject('test');
        $this->assertEquals($email->to('bob@example.com', 'Bob'), array('Bob <bob@example.com>'));
        $this->assertEquals($email->from('dave@example.com', 'Dave'), 'Dave <dave@example.com>');
        $email->setMessage('Hello Bob');
        $str = (string)$email;
        $this->assertEmailHeader($str, 'Content-Type', 'text/plain;charset=ISO-8859-1');
        $this->assertEmailHeader($str, 'To', 'Bob <bob@example.com>');
        $this->assertEmailHeader($str, 'From', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Reply-To', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Return-Path', '<dave@example.com>');
    }

    /**
     * A simple HTML email
     */
    function testSimpleHtml(){
        $email = new Breezemail();
        $email->id('test-html-email');
        $email->contentType('text/html');
        $email->subject('test');
        $this->assertEquals($email->to('bob@example.com', 'Bob'), array('Bob <bob@example.com>'));
        $this->assertEquals($email->from('dave@example.com', 'Dave'), 'Dave <dave@example.com>');
        $email->setMessage('<p>Hello <b>Bob</b></p>');
        $str = (string)$email;
        $this->assertEmailHeader($str, 'Content-Type', 'text/html;charset=ISO-8859-1');
        $this->assertEmailHeader($str, 'To', 'Bob <bob@example.com>');
        $this->assertEmailHeader($str, 'From', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Reply-To', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Return-Path', '<dave@example.com>');
    }
    /**
     * Missing a from name should be OK and adds angles arounf Return-Path
     * and Return-Receipt-To
     */
    function testMissingFromName(){
        $email = new Breezemail();
        $email->id('test-text-email');
        $email->subject('test');
        $this->assertEquals($email->to('bob@example.com', 'Bob'), array('Bob <bob@example.com>'));
        $this->assertEquals($email->from('dave@example.com'), 'dave@example.com');
        $email->setMessage('Hello Bob');
        $str = (string)$email;
        $this->assertEmailHeader($str, 'From', 'dave@example.com');
        $this->assertEmailHeader($str, 'Reply-To', 'dave@example.com');
        $this->assertEmailHeader($str, 'Return-Path', '<dave@example.com>');
    }
    /**
     * Giving content in HTML or falling back to text with alternative
     * content - do I even want this? Normally you just pass in a
     * message to send and set encoding of body before or not.
     */
    function testMultipartAlternative(){

    }

    /**
     * Attaching a file
     */
    function testAttachment(){
        $email = new Breezemail();
        $email->id('test-single_attachment-email');
        $email->subject('Test with attachment');
        $this->assertEquals($email->to('bob@example.com', 'Bob'), array('Bob <bob@example.com>'));
        $this->assertEquals($email->from('dave@example.com', 'Dave'), 'Dave <dave@example.com>');
        $email->setMessage('Hello Bob, please find one file attached...');
        $email->attachFile(__DIR__.'/chart_bar.png', 'image/png');
        $str = (string)$email;
        $this->assertEmailHeader($str, 'Content-Type', 'multipart/mixed;boundary="' . md5($email->id()) . '"');
        $this->assertEmailHeader($str, 'To', 'Bob <bob@example.com>');
        $this->assertEmailHeader($str, 'From', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Reply-To', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Return-Path', '<dave@example.com>');
    }

    /**
     * Attaching multiple files of different types
     */
    function testMultipleAttachments(){
        $email = new Breezemail();
        $email->id('test-multi-attachment-email');
        $email->subject('Test with multiple attachments');
        $this->assertEquals($email->to('bob@example.com', 'Bob'), array('Bob <bob@example.com>'));
        $this->assertEquals($email->from('dave@example.com', 'Dave'), 'Dave <dave@example.com>');
        $email->setMessage('Hello Bob, please find a couple of files attached...');
        $email->attachFile(__DIR__ . '/chart_bar.png', 'image/png');
        $email->attachFile(__DIR__ . '/loading.gif', 'image/gif');
        $str = (string)$email;
        $this->assertEmailHeader($str, 'Content-Type', 'multipart/mixed;boundary="' . md5($email->id()) . '"');
        $this->assertEmailHeader($str, 'To', 'Bob <bob@example.com>');
        $this->assertEmailHeader($str, 'From', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Reply-To', 'Dave <dave@example.com>');
        $this->assertEmailHeader($str, 'Return-Path', '<dave@example.com>');
    }
}
