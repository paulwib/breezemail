<?php

/**
 * A simple class for constructing and sending emails
 */

class Breezemail {

    protected $newline_char = "\r\n";
    protected $to = array();
    protected $cc = array();
    protected $bcc = array();
    protected $from = false;
    protected $reply = false;
    protected $subject = false;
    protected $message = false;
    protected $content_type = 'text/plain';
    protected $encoding = 'ISO-8859-1';
    protected $attachments = array();
    protected $boundary_marker = false;
    protected $id = false;

    /**
     * Validate email
     * @params string $str The string to check is a valid email
     * @return boolean
     */
    static function validEmail($str) {
        $rx = '/^[a-z0-9]+([_\+\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i';
        return preg_match($rx, $str) ? true : false;
    }
    /**
     * Set/get newline character
     *
     * @param string $str
     */
    public function newline($str=false) {
        if($str) {
            $this->newline_char = $str;
        }
        return $this->newline_char;
    }

    /**
     * Add To recipient
     * @params string $email Required, valid email address
     * @params string $name Optional, default false
     * @return boolean False if email invalid, or contents of To
     */
    public function to($email=false, $name=false){
        return $this->addRecipient($email, $name, 'To');
    }

    /**
     * Add Cc recipient
     * @params string $email Required, valid email address
     * @params string $name Optional, default false
     * @return boolean False if email invalid, or contents of Cc
     */
    public function cc($email=false, $name=false){
        return $this->addRecipient($email, $name, 'Cc');
    }

    /**
     * Add Bcc recipient
     * @params string $email Required, valid email address
     * @params string $name Optional, default false
     * @return boolean False if email invalid, or contents of Bcc
     */
    public function bcc($email=false, $name=false){
        return $this->addRecipient($email, $name, 'Bcc');
    }

    /**
     * Set/get recipients
     * @params string $email Optional, valid email address
     * @params string $name Optional, default false
     * @params string $header_type Optional, one of 'To', 'Cc' or 'Bcc',
     * default is To
     * @return mixed False if bad email or contents of header type
     */
    public function addRecipient($email=false, $name=false, $header_type='To'){
        if ($email) {
            if (!Breezemail::validEmail($email)) {
                return false;
            }
            $this->{strtolower($header_type)}[] = $name ? "$name <$email>" : $email;
        }
        return $this->{strtolower($header_type)};
    }
    /**
     * Remove a recipent from all recipent headers
     * @param string $email The email address to remove
     * @return void
     */
    public function removeRecipient($email){
        $tmp = array();
        foreach (array('to','cc','bcc') as $header_type){
            foreach ($this->{$header_type} as $recpt) {
                if (!stristr($recpt, $email)) {
                    $tmp[] = $rcpt;
                }
            }
            $this->{$header_type} = $tmp;
        }
    }

    /**
     * Set/get From
     * @params string $email Required, valid email address
     * @params string $name Optional
     * @return mixed Boolean false if email invalid, current From address if
     * added or called without any arguments
     */
    public function from($email=false, $name=false){
        if ($email) {
            if (!Breezemail::validEmail($email)) {
                return false;
            }
            $this->from = $name ? "$name <$email>" : $email;
        }
        return $this->from;
    }

    /**
     * Set/get Reply (defaults to From if you don't touch this)
     * @params string $email Required, valid email address
     * @params string $name Optional
     * @return mixed Boolean false if email invalid, current Reply address if
     * added or called without any arguments
     */
    public function reply($email=false, $name=false){
        if ($email) {
            if (!Breezemail::validEmail($email)) {
                return false;
            }
            $this->reply = $name ? "$name <$email>" : $email;
        }
        return $this->reply ? $this->reply : $this->from;
    }

    /**
     * Set/get subject line
     * @params string $str Optional, the subject
     * @return mixed The current Subject line
     */
    public function subject($str=false){
        if ($str) {
            $this->subject = $str;
        }
        return $this->subject;
    }

    /**
     * Set message body
     */
    public function setMessage($str){
        $this->message = $str;
    }

    /**
     * Set/get content-type
     * @params string $str Optional, the content-type
     * @return mixed The current content-type
     */
    public function contentType($str=false){
        if ($str) {
            $this->content_type = $str;
        }
        return $this->content_type;
    }

    /**
     * Set/get encoding
     * @params string $str Optional, the encoding
     * @return mixed The current encoding
     */
    public function encoding($str=false){
        if ($str) {
            $this->encoding = $str;
        }
        return $this->encoding;
    }

    /**
     * Get message id
     * @param string $force_id Force a particular id to be used
     * @return string A unique message id (hopefully a UUID?)
     */
    public function id($force_id=false){
        if (!$this->id) {
            $this->id = $force_id ?
                $force_id : md5(date('c') . rand(1,10)) . '@' .  @$_SERVER['SERVER_NAME'];
        }
        return $this->id;
    }

    /**
     * Get boundary marker for multipart messages
     *
     * @param bool $dashes Wheteher to precede marker with dashes, default true - when getting for
     *  header this should be false, in body of message should be true.
     * @return string A unique boundary, based on id
     */
    public function boundaryMarker($dashes=true){
        if (!$this->boundary_marker) {
            $this->boundary_marker = md5($this->id());
        }
        return  ($dashes ? '--' : '') . $this->boundary_marker;
    }

    /**
     * Attach a file to the email
     * @param string $f Path to file
     * @param string $mime_type Mime type of the file
     * @return bool True on success false on failure
     */
    public function attachFile($f, $content_type){
        $this->attachments[] = array(
            'filename'=>basename($f),
            'content_type'=>$content_type,
            'blob'=>chunk_split(base64_encode(file_get_contents($f)), 76, $this->newline()),
            'content_transfer_encoding'=>'base64',
        );
    }

    /**
     * Get email headers based on current data. The From address,
     * Return-Path and Return-Receipt-To headers are set, but might
     * not work depending on sendmail - attempts to fix this in
     * send() by forcing paramaters @see send
     * @return array One header per element
     */
    public function headers(){
        $return_path = $this->from();
        $return_path = stristr($return_path, '<') ?
            preg_replace('/^[^<]+/', '', $return_path) : '<' . $return_path . '>';
        $headers = array(
            'Return-Path: '       . $return_path,
            'MIME-Version: 1.0',
            'Date: '              . date('r'),
            'Message-ID: '        . '<'. $this->id() . '>',
            'From: '              . $this->from(),
            'Reply-To: '          . $this->reply(),
        );
        foreach ($this->cc as $e) {
            $headers[] = 'Cc: ' . $e;
        }
        foreach ($this->bcc as $e) {
            $headers[] = 'Bcc: ' . $e;
        }
        if ($this->attachments) {
            $headers[] = 'Content-Type: multipart/mixed;boundary="' . $this->boundaryMarker(false) . '"';
        } else {
            $headers[] = 'Content-Type: ' . $this->contentType() . ';charset=' . $this->encoding();
        }
        return $headers;
    }

    /**
     * Check we have everything ready to send the message
     * @return bool
     */
    public function validate(){
        if (!$this->to()) {
            return false;
        }
        if (!$this->subject()) {
            return false;
        }
        if (!$this->from()) {
            return false;
        }
        return true;
    }
    /**
     * Prepare the message for sending. Will wrap plain text and add
     * any attachments to the message body setting boundaries
     * as appropriate.
     * @param string $message The message
     * @return string The prepared message
     */
    public function prepare(){
        $message = $this->message;
        if ($this->contentType() == 'text/plain') {
            $message = wordwrap($message, 75, $this->newline());
        }
        if ($this->attachments) {
            $boundary = $this->boundaryMarker(true);
            $tmp = array(
                'This is a multi-part message in MIME format.',
                $boundary,
                'Content-type: ' . $this->contentType() . ';charset=' . $this->encoding(),
                'Content-transfer-encoding: 7bit',
                '',
                $message,
            );
            foreach ($this->attachments as $a) {
                $tmp[] = '';
                $tmp[] = $boundary;
                $tmp[] = 'Content-Type: ' . $a['content_type'] . ";name=" . $a['filename'];
                $tmp[] = 'Content-Disposition: attachment;filename=' . $a['filename'];
                $tmp[] = 'Content-Transfer-Encoding: ' . $a['content_transfer_encoding'];
                $tmp[] = '';
                $tmp[] = $a['blob'];
                $tmp[] = '';
            }
            $tmp[] = "$boundary--";
            return implode($this->newline(), $tmp);
        }
        return $message;
    }

    /**
     * Send the message
     * @params bool $fix_env Fix the From: and Return-Path: on the
     * envelope, which you might need on Linux servers to avoid messages
     * being rejected into a black hole somewhere.
     *
     * Depending on your sendmail setup you may find messages get a
     * header added like:
     *
     * X-Authentication-Warning: local.lan: user set sender to
     *  <foo@example.com> using -f
     *
     * That's added because the user running the script that invokes
     * mail() is not a trusted user. To add trusted users you need to
     * put user names in /etc/mail/trusted-users.
     *
     * The edit /etc/mail/sendmail.mc and add:
     *
     * FEATURE(`use_ct_file')
     *
     * Needs to go somewhere before first call to MAILER. Then run:
     *
     * sudo sendmailconfig
     *
     * To re-generate config and restart sendmail
     *
     * If that still doesn't work, try adding the same FEATURE line
     * near the top of /etc/mail/submit.mc and run sendmailconfig
     * again.
     *
     * If that still doesn't work consult the sendmail manual ;)
     *
     * @return bool Result of call to mail()
     */
    public function send($fix_env=true){
        if (!$this->validate()) {
            throw new InvalidArgumentException('Email is not valid');
        }
        $to = implode(',', $this->to());
        $message = $this->prepare();
        $headers = implode($this->newline(), $this->headers());
        if ($fix_env) {
            $env_from = preg_replace('/^[^<]+/', '', $this->from());
            return mail($to, $this->subject(), $message, $headers, "-f $env_from");
        }
        return mail($to, $this->subject(), $message, $headers);
    }

    /**
     * Convert to string
     * Can be saved to a file so you can check message looks correct in
     * various clients, save the message for sending later etc.
     */
    public function __toString(){
        // Add Subject: and To: header (normally added by the mail() call)
        $headers = $this->headers();
        $headers[] = 'To: ' . implode(', ', $this->to());
        $headers[] = 'Subject: ' . $this->subject();
        $header_str = implode($this->newline(), $headers);
        $message = $this->prepare();
        return $header_str . $this->newline() . $this->newline() . $message;
    }
}
